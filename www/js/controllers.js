angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout, $http) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  // Form data for the config modal
  $scope.configData = {
    refreshtime: 20,
    disabletime: 240
  };
  // user tracking location data
  $scope.trackingData = {
    totalsent: 0,
    totalfailed: 0
  };
  //load saved values
  if(window.localStorage.getItem("userinbox") !== undefined) $scope.configData.userinbox = window.localStorage.getItem("userinbox");
  if(window.localStorage.getItem("userid") !== undefined) $scope.configData.userid = window.localStorage.getItem("userid");
  if(window.localStorage.getItem("refreshtime") !== undefined) $scope.configData.refreshtime = window.localStorage.getItem("refreshtime");
  if(window.localStorage.getItem("disabletime") !== undefined) $scope.configData.disabletime = window.localStorage.getItem("disabletime");

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/config.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeConfig = function() {
    $scope.modal.hide();
  };

  // Open the config modal
  $scope.config = function() {
    if ($scope.configData.refreshtime == undefined) $scope.configData.refreshtime = 20;
    if ($scope.configData.disabletime == undefined) $scope.configData.disabletime = 240;
    $scope.modal.show();
  };

  $scope.switchoff = function() {
    if ($scope.trackingData.activetracking !== undefined) $scope.trackingData.activetracking = false;
    if ($scope.trackingData.position !== undefined) $scope.trackingData.position.res = null;
    if ($scope.trackingData.lasterror !== undefined) $scope.trackingData.lasterror = null;
    
    // // disable application if we're still in background
    cordova.plugins.backgroundMode.disable();
  };

  $scope.doConfig = function() {
    if ($scope.configData.userinbox !== undefined) window.localStorage.setItem("userinbox", $scope.configData.userinbox);
    if ($scope.configData.userid !== undefined) window.localStorage.setItem("userid", $scope.configData.userid);
    if ($scope.configData.refreshtime !== undefined) window.localStorage.setItem("refreshtime", $scope.configData.refreshtime);
    if ($scope.configData.disabletime !== undefined) window.localStorage.setItem("disabletime", $scope.configData.disabletime);
    $scope.trackingData.missingconfiguration = false;

    $scope.switchoff(); // stop if running to allow start it again with new values
    // Simulate a delay.
    $timeout(function() {
      $scope.closeConfig();
    }, 150);
  };

  $scope.sendlocation = function() {

    $http({
        method: 'POST',
        url: 'https://qwasap.com/api/v2/index.php/notificationmanager/directcall',
        data: { 
          userinbox: $scope.configData.userinbox, 
          userid: $scope.configData.userid,
          lat: $scope.trackingData.position.data.coords.latitude,
          lon: $scope.trackingData.position.data.coords.longitude,
          address: $scope.trackingData.address
        }
    }).then(function successCallback(response) {
        $scope.trackingData.totalsent += 1;
        var currentdate = new Date(); 
        $scope.trackingData.timelastsent = ('0'+currentdate.getDate()).slice(-2) + "/"
              + ('0'+(currentdate.getMonth()+1)).slice(-2) + "/" 
              + currentdate.getFullYear() + " @ "  
              + ('0'+currentdate.getHours()).slice(-2) + ":"  
              + ('0'+currentdate.getMinutes()).slice(-2);
              // + ":" + ('0'+currentdate.getSeconds()).slice(-2);
    }, function errorCallback(response) {
        $scope.trackingData.totalfailed += 1;
        $scope.trackingData.lasterror = response;
    });
  };

})

.controller('ConfigureController', function($scope) {

  this.buildTimeString = function buildTimeString(time) 
  {
    hours = Math.floor(time / 60);
    minutes = time - (hours * 60);

    if (hours > 0)
    {
      timestr = hours + " hora";
      if (hours > 1) timestr += "s";
    }
    else
      timestr = "";

    if (hours > 0 && minutes > 0)
      timestr = timestr + " y ";

    if (minutes > 0)
      timestr = timestr + minutes + " minutos";

    return timestr;
  };

  this.buildShortTimeString = function buildShortTimeString(time) 
  {
    hours = Math.floor(time / 60);
    minutes = time - (hours * 60);
    var timestr = ("0" + hours).slice(-2) + ":" + ("0" + minutes).slice(-2);
    return timestr;
  };

})

.controller('HelpCtrl', function($scope) {

})

.controller('TrackingCtrl', function($scope, $cordovaGeolocation, $interval, $http) {

  $scope.getLocation = function getLocation($cordovaGeolocation) 
  {
    var posOptions = {timeout: 10000, maximumAge: 60000, enableHighAccuracy: false};

    $cordovaGeolocation
    .getCurrentPosition(posOptions)
    .then(function (position) {

      /*** position example ***
      position = {
        "timestamp": 1471626646410,
        "coords": {
          "accuracy": 175,
          "latitude": 38.7518914,
          "altitudeAccuracy": null,
          "heading": null,
          "longitude": -0.7199875,
          "speed": null,
          "altitude": null
        } 
      }
      */

      $scope.trackingData.position = {
        res: 'ok',
        msg: 'position retrieved',
        data: position
      }
      $scope.trackingData.address = '';

      $http({
          method: 'POST',
          url: 'http://maps.googleapis.com/maps/api/geocode/json?latlng='+position.coords.latitude+','+position.coords.longitude+'&sensor=true',
          data: {}
      }).then(function successCallback(response) {
          var index = 0;
          if (response.hasOwnProperty("status") && response.status == 200)
          {
            var results = response.data.results;
            if (results[2]) index = 2;
            else if (results[1]) index = 1;
            else index = 0;
            var address = results[index]['formatted_address'];
            $scope.trackingData.address = address;
            $scope.sendlocation();
          }
      }, function errorCallback(response) {
          $scope.sendlocation();
      });

    }, function(err) {
      // err.message = "The last location provider was disabled" => this means that user does not allow android to retrieve location info
     
      $scope.trackingData.position = {
        res: 'error',
        msg: err.message,
        data: "No se ha podido recuperar la posición. Por favor, asegúrate de que en la ajustes de ubicación de tu dispositivo has autorizado que las aplicaciones puedan usar los datos de ubicación y/o los datos de GPS."
      }
    });
  
    $scope.trackingData.requestleft -= 1;
    if ($scope.trackingData.requestleft <= 0) $scope.switchoff();
  };

  this.switchTracking = function switchTracking()
  {
    if ($scope.trackingData.activetracking) 
    {
      var minTimeMsAllowed = 5 * 60 * 1000; // 5 minutes
      var maxTimeMsAllowed = 12 * 60 * 60 * 1000; // 12 hours

      delayms = $scope.configData.refreshtime * 60 * 1000;
      if (delayms < minTimeMsAllowed) delayms = minTimeMsAllowed; 
      if (delayms > maxTimeMsAllowed) delayms = minTimeMsAllowed; 
      // delayms = 20000; // test at 20 seconds

      disablems = $scope.configData.disabletime * 60 * 1000;
      if (disablems < minTimeMsAllowed) disablems = minTimeMsAllowed; 
      if (disablems > maxTimeMsAllowed) disablems = maxTimeMsAllowed; 
      
      if ($scope.configData.userinbox !== undefined && $scope.configData.userinbox !== null && $scope.configData.userinbox !== 'null' && $scope.configData.userid !== undefined && $scope.configData.userid !== null && $scope.configData.userid !== 'null')
      {
        count = Math.floor(disablems / delayms);
        $scope.trackingData.totalsent = 0;
        $scope.trackingData.totalfailed = 0;
        $scope.getLocation($cordovaGeolocation);
        $scope.trackingData.requestleft = count;

        // Enable background mode while track is playing
        cordova.plugins.backgroundMode.setDefaults({
          title:  'Qwasap trabajará en segundo plano',
          text:   'Seguimiento de posición',
          ticker: 'Localizando en segundo plano',
        });
        cordova.plugins.backgroundMode.enable();

        $scope.trackingData.promise = $interval(function() {
          $scope.getLocation($cordovaGeolocation);
        }, delayms, count);        
      } 
      else
      {
        $scope.trackingData.missingconfiguration = true;
      }

    }
    else
    {
      // disable application if we're still in background
      cordova.plugins.backgroundMode.disable();
      
      $interval.cancel($scope.trackingData.promise);
      $scope.switchoff();
    }
  }

})
